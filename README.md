# Bikes!
Web site using Flask and psycopg2 managing and showing off data coming from a LoRa device. 

## Setting up
- cd into the directory
- virtualenv venv
- source venv/bin/activate
- pip install Flask
- pip install psycopg2
- pip install psycopg2-binary

## Run
FLASK_ENV="development" python main.py

## todo
- jinja2 templating (use/include parts)
- maps?
- menu on the top
- fix database with the new table schema (iotdata, bikes)
