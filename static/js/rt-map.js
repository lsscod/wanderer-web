let map = L.map('map').setView([56.517, 10.255], 5);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

let shipLayer = L.layerGroup();
let bike = L.icon({
    iconUrl: '/static/bike32.png',
    iconSize: [32, 32]
});
let bikeAlert = L.icon({
    iconUrl: '/static/bike-alert32-shadow2.png',
    iconSize: [48, 48]
});

let realtime = L.realtime({
    url: '/map_data',
    crossOrigin: true,
    type: 'text'
}, {
    interval: 10 * 1000,
    getFeatureId: function (featureData) {
        return featureData.properties.serial + " " + Math.random();
    },
    pointToLayer: function (feature, latlng) {
        if (feature.properties.status === 1)
            marker = L.marker(latlng, {icon: bikeAlert});
        else marker = L.marker(latlng, {icon: bike});
        marker.bindPopup('serial: ' + feature.properties.serial +
            '<br/> lat: ' + feature.properties.x +
            '<br/> lng: ' + feature.properties.y +
            '<br/> status: ' + feature.properties.status +
            '<br/> customer: ' + feature.properties.customer);
        marker.addTo(shipLayer);
        return marker;
    }

}).addTo(map);
realtime.on('update', function () {
    map.fitBounds(realtime.getBounds(), {maxZoom: 10});
});
